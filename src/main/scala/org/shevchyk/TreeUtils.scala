package org.shevchyk

object TreeUtils {

  def sortNodeLeaves[A](maxWeight: A, node: Node[A], spare: List[Leaf[A]])(implicit c: Numeric[A]): (Node[A], List[Leaf[A]]) ={
    val sortedLeaves = sortLeaves(node.leaves ++ spare)
    val (leafs, left) = splitLeaves(sortedLeaves, maxWeight)

    val (sortedChildren, leftLeafs) = sortChildrenLeaves(maxWeight, node.children, left)
    (Node(leafs, sortedChildren), leftLeafs)
  }

  def sortChildrenLeaves[A](maxWeight: A, nodes: List[Node[A]], leftLeaves: List[Leaf[A]])(implicit c: Numeric[A]): (List[Node[A]], List[Leaf[A]]) ={
    nodes match {
      case Nil => (nodes, leftLeaves)
      case node::tail =>
        val (n, leaves) = sortNodeLeaves(maxWeight, node, leftLeaves)
        val (nodes, left) = sortChildrenLeaves(maxWeight, tail, leaves)
        (n::nodes, left)
    }
  }

  def splitLeaves[A](list: List[Leaf[A]], maxWeight: A)(implicit c: Numeric[A]): (List[Leaf[A]], List[Leaf[A]]) = {
    def split(leaves: List[Leaf[A]], leftLeaves: List[Leaf[A]], weight: A): (List[Leaf[A]], List[Leaf[A]]) = {
      leftLeaves match {
        case h::tail if c.compare(weight, h.weight) >= 0 => split(leaves :+ h, tail, c.minus(weight, h.weight))
        case _ => (leaves, leftLeaves)
      }
    }
    split(Nil, list, maxWeight)
  }

  def sortLeaves[A](head: List[Leaf[A]])(implicit c: Numeric[A]): List[Leaf[A]] = {
    head match {
      case Nil | _::Nil => head
      case _ =>
        val (a, b) = head.splitAt(head.length/2)
        merge(sortLeaves(a), sortLeaves(b))
    }
  }

  def merge[A](left: List[Leaf[A]], right: List[Leaf[A]])(implicit c: Numeric[A]): List[Leaf[A]] = {
    (left, right) match {
      case ( h1::tail1 , h2::tail2) => if (c.compare(h1.weight, h2.weight) < 0) h1::merge(right, tail1) else h2::merge(tail2, left)
      case ( a, Nil) => a
      case (Nil, b) => b
    }
  }
}

case class Leaf[A](weight: A)(implicit c: Numeric[A]){
  assert(c.compare(weight, c.zero) > 0, "Must be greater than 0")
}
case class Node[A](leaves: List[Leaf[A]], children: List[Node[A]])(implicit c: Numeric[A]){
  def sort(weight: A): Node[A] = {
    TreeUtils.sortNodeLeaves[A](weight, this, List())._1
  }
}

