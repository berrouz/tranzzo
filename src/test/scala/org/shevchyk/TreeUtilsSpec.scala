package org.shevchyk

import org.scalatest.MustMatchers
import org.shevchyk.TreeUtils._

class TreeUtilsSpec extends org.scalatest.FunSuite with MustMatchers{

  val node: Node[Int] = Node(notSortedLeafs, List(
    Node[Int](List(), List()),
    Node[Int](List(), List()),
    Node[Int](List(), List())))

  val nodeWithFloats: Node[Float] = Node(floatLeafs, List(
    Node[Float](List(), List()),
    Node[Float](List(), List()),
    Node[Float](List(), List())))

  test("merge sorted lists"){
    merge(Leaf(4)::Leaf(5)::Nil, Leaf(1)::Leaf(2)::Nil) mustBe Leaf(1)::Leaf(2)::Leaf(4)::Leaf(5)::Nil
    merge[Int](Nil, Nil) mustBe Nil
    merge(Nil, Leaf(1)::Nil) mustBe Leaf(1)::Nil
    merge(Leaf(1)::Nil, Nil) mustBe Leaf(1)::Nil
  }

  test("sort list"){
    sortLeaves(Leaf(4)::Leaf(3)::Leaf(5)::Nil) mustBe Leaf(3)::Leaf(4)::Leaf(5)::Nil
  }

  test("must split into 2 lists when weight is 0"){
    splitLeaves(leafs, 10) mustBe (leafs, Nil)
    splitLeaves(leafs, 3) mustBe (leafs.take(2), leafs.takeRight(2))
  }

  test("sort leaves"){
    sortNodeLeaves(3, node, List())._1 mustBe Node(Leaf(1)::Leaf(2)::Nil,
      List(
        Node[Int](Leaf(3)::Nil, List()),
        Node[Int](Nil, List()),
        Node[Int](Nil, List())
      ))
  }

  test("sort node leaves with int"){
   node.sort(3) mustBe Node(Leaf(1)::Leaf(2)::Nil,
      List(
        Node[Int](Leaf(3)::Nil, List()),
        Node[Int](Nil, List()),
        Node[Int](Nil, List())
      ))
  }

  test("sort node leaves with float"){
    nodeWithFloats.sort(3.0f) mustBe Node(Leaf(1.5f)::Nil,
      List(
        Node[Float](Leaf(2.0f)::Nil, List()),
        Node[Float](Nil, List()),
        Node[Float](Nil, List())
      ))
  }

  def leafs: List[Leaf[Int]] = {
    Leaf(1)::Leaf(2)::Leaf(3)::Leaf(4)::Nil
  }

  def notSortedLeafs: List[Leaf[Int]] = {
    Leaf(2)::Leaf(4)::Leaf(3)::Leaf(1)::Nil
  }

  def floatLeafs: List[Leaf[Float]] = {
    Leaf(1.5f)::Leaf(2.0f)::Leaf(4.0f)::Leaf(5.0f)::Nil
  }
}
